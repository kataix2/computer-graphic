#pragma once

// GLEW
#include <GL/glew.h>

// SDL
#include <SDL.h>
#include <SDL_opengl.h>

// GLM
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtx/transform2.hpp>

#include "gVertexBuffer.h"
#include "gShaderProgram.h"
#include "gCamera.h"
#include "Mesh_OGL3.h"
#include <functional>

class CMyApp
{
public:
	CMyApp(void);
	~CMyApp(void);

	bool Init();
	void Clean();

	void Update();
	void Render();

	void KeyboardDown(SDL_KeyboardEvent&);
	void KeyboardUp(SDL_KeyboardEvent&);
	void MouseMove(SDL_MouseMotionEvent&);
	void MouseDown(SDL_MouseButtonEvent&);
	void MouseUp(SDL_MouseButtonEvent&);
	void MouseWheel(SDL_MouseWheelEvent&);
	void Resize(int, int);
protected:

	GLuint GenTexture(int r,int g,int b);


	void add_triangle(const glm::vec3& P1,
		const glm::vec3& P2,
		const glm::vec3& P3,
		gVertexBuffer& buffer);
	bool initHeadWheelCover(gVertexBuffer buffer[], int size, double radius );
	bool initCylinder(gVertexBuffer &buffer);

	
	GLuint m_texTrain;
	GLuint m_cowtext;
	GLuint m_texWheel;
	GLuint m_texture_gen;

	gCamera			m_camera;
	gShaderProgram	m_program;


	int active_octa = 0;
	int cow_number = 0;

	float wheelspace = 0.1; 

	double radiusOfTrainHeadWheel = 0.15;
	double heightOfTrainHeadWheel = 0.1;
	
	double radiusOfWagonWheel = 0.125;
	double heightOfWagonWheel = 0.1;

	int faces = 360;

	gVertexBuffer	m_vb; //plane
	gVertexBuffer   m_prism; // train
	gVertexBuffer   m_cabin; // cabin
	gVertexBuffer   m_line1;  // line 1
	gVertexBuffer	m_line2;	// line 2
	gVertexBuffer	m_line3;  // line 3
	gVertexBuffer	m_line4; // line 4
	gVertexBuffer	m_wagon; // wagon
	gVertexBuffer	m_wheel; // wheel
	gVertexBuffer	m_wheels[6]; //train head wheel;
	gVertexBuffer	m_WagonWheels[8]; // wagon wheel;
	gVertexBuffer	m_wheelCovers[12]; // head wheel cover;
	gVertexBuffer	m_wagonWheelCovers[16]; // wagon wheel cover;
	bool depart = false;
	int time = 0;
	
	gVertexBuffer m_vb_circle;
	
	
	float zpos = 0;
	//float wheeldelay = 0.025; < 2.5
	float wheeldelay = 0.030; // < 3.1
	//float wheeldelay = 0.031;//3.5
	//float wheeldelay = 0.006; // 4
	float speedFactor = 2.5; 

	Mesh *cows[6]; //cows 
	// light color
	glm::vec4 m_Ld = glm::vec4(0.788f, 0.886f, 1, 1);
	
};

