#include "MyApp.h"
#include "GLUtils.hpp"

#include <GL/GLU.h>
#include <math.h>

#include "ObjParser_OGL3.h"

CMyApp::CMyApp(void)
{
	m_texture_gen = 0;
	m_texTrain = 0; 
	m_cowtext = 0;
	m_texWheel = 0;
}


CMyApp::~CMyApp(void)
{
}

GLuint CMyApp::GenTexture(int r,int g,int b)
{
	unsigned char tex[256][256][3];

	for (int i = 0; i < 256; ++i)
		for (int j = 0; j < 256; ++j)
		{
			tex[i][j][0] = r;
			tex[i][j][1] = g;
			tex[i][j][2] = b;
		}

	GLuint tmpID;

	glGenTextures(1, &tmpID);
	glBindTexture(GL_TEXTURE_2D, tmpID);
	gluBuild2DMipmaps(GL_TEXTURE_2D,
		GL_RGB8, 256, 256,			// define storage on the GPU: RGB, 8bits per channel, 256x256 texture
		GL_RGB, GL_UNSIGNED_BYTE,	// define storate in RAM: RGB layout, unsigned bytes for each channel
		tex);						//						  and source pointer
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);	// bilinear filter on min and mag
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
	glBindTexture(GL_TEXTURE_2D, 0);

	return tmpID;
}


bool CMyApp::initHeadWheelCover(gVertexBuffer buffer[],int size , double radius) {
	
	for (int i = 0; i < size; i++)
	{
		buffer[i].AddAttribute(0, 3); // positions
		buffer[i].AddAttribute(1, 3); // normals
		buffer[i].AddAttribute(2, 2); // texture
		float x, y, z;


		for (int j = 0; j <= faces; j++) {
			double u = j / (double)faces;
			x = radius * cos(2 * M_PI * u);
			z = radius * sin(2 * M_PI * u);
			// Bottom vertex

			buffer[i].AddData(0, x, 0, z);
			buffer[i].AddData(1, 0, 1, 0);
			buffer[i].AddData(2, x, z);
		}
		buffer[i].InitBuffers();
	}
	return true;
}

bool CMyApp::initCylinder(gVertexBuffer& buffer)
{
	buffer.AddAttribute(0, 3); // positions
	buffer.AddAttribute(1, 3); // normals
	buffer.AddAttribute(2, 2); // texture

	float halfLength = heightOfTrainHeadWheel / 2;

	for (int i = 0; i < 360; i++)
	{
	//	double u = i / (double)720;
		float theta = (((float)i) * 2.0 * M_PI) / 360;
		float nextTheta =  (((float) i + 1) * 2.0 * M_PI) / 360; 

		buffer.AddData(0, 0,halfLength , 0);

		buffer.AddData(0,radiusOfTrainHeadWheel * cos(theta), halfLength, radiusOfTrainHeadWheel * sin(theta));
		buffer.AddData(0,radiusOfTrainHeadWheel * cos(nextTheta), halfLength, radiusOfTrainHeadWheel * sin(nextTheta));
		
		buffer.AddData(0,radiusOfTrainHeadWheel * cos(nextTheta), -halfLength, radiusOfTrainHeadWheel * sin(nextTheta));
		buffer.AddData(0,radiusOfTrainHeadWheel * cos(theta), -halfLength, radiusOfTrainHeadWheel * sin(theta));
		buffer.AddData(0, 0, -halfLength, 0);
		buffer.AddData(1, 0, 1, 0);
		buffer.AddData(2, 0, 0);
		buffer.AddData(2, 0, halfLength);
		buffer.AddData(2, radiusOfTrainHeadWheel * cos(theta), radiusOfTrainHeadWheel * sin(theta));
		buffer.AddData(2, radiusOfTrainHeadWheel * cos(nextTheta), radiusOfTrainHeadWheel * sin(nextTheta));
		buffer.AddData(2, 0, 0);
		buffer.AddData(2, 0, -halfLength);
	}
	buffer.InitBuffers();

	return true;
}

void CMyApp::add_triangle(
	const glm::vec3& P1,
	const glm::vec3& P2,
	const glm::vec3& P3,
	gVertexBuffer& buffer) {

	buffer.AddData(0, P1); // P1
	buffer.AddData(0, P2.x, P2.y, P2.z); // P2
	buffer.AddData(0, P3.x, P3.y, P3.z); // P3

	glm::vec3 V1 = P2 - P1; //P2-P1
	glm::vec3 V2 = P3 - P1; //P3-P1
	glm::vec3 normal = glm::normalize(glm::cross(V1, V2));

	//normals
	buffer.AddData(1, normal.x, normal.y, normal.z);
	buffer.AddData(1, normal.x, normal.y, normal.z);
	buffer.AddData(1, normal.x, normal.y, normal.z);

	//texture coordinates
	buffer.AddData(2, 0, 0);
	buffer.AddData(2, 0.5, 1);
	buffer.AddData(2, 1, 0);
}

bool CMyApp::Init()
{
	// set the clear color (background color)
	glClearColor(0.502f, 0.502f, 0.502f, 1.0f);

	//glEnable(GL_CULL_FACE);		// enable backface culling
	glEnable(GL_DEPTH_TEST);	// enable depth test (z-buffer)
	//glCullFace(GL_BACK);
	
	
	

	//head wheel cover

	initHeadWheelCover(m_wheelCovers, 12 , radiusOfTrainHeadWheel);
	
	//wagon wheel cover

	initHeadWheelCover(m_wagonWheelCovers, 16 , radiusOfWagonWheel);

	//wagon wheel

	for (int i = 0; i < 8; i++) {
		double x, y, z;
		y = heightOfTrainHeadWheel;
		m_WagonWheels[i].AddAttribute(0, 3); //position
		m_WagonWheels[i].AddAttribute(1, 3); //normals
		m_WagonWheels[i].AddAttribute(2, 2); //texture
		for (int j = 0; j <= faces; j++) {
			double u = j / (double)faces;
			x = radiusOfWagonWheel * cos(2 * M_PI * u);
			z = radiusOfWagonWheel * sin(2 * M_PI * u);
			// Bottom vertex
			m_WagonWheels[i].AddData(0, x, 0, z);
			m_WagonWheels[i].AddData(1, 0, 1);
			m_WagonWheels[i].AddData(2, u, 1);
			// Top vertex
			m_WagonWheels[i].AddData(0, x, y, z);
			m_WagonWheels[i].AddData(1, 0, 0);
			m_WagonWheels[i].AddData(2, u, 0);

		}
		m_WagonWheels[i].InitBuffers();
	}

	
	//train head wheel
	for (int i = 0; i < 6; i++) {
		double x, y, z;
		y = heightOfTrainHeadWheel;
		m_wheels[i].AddAttribute(0, 3); //position
		m_wheels[i].AddAttribute(1, 3); //normals
		m_wheels[i].AddAttribute(2, 2); //texture
		for (int j = 0; j <= faces; j++) {
			double u = j / (double)faces;
			x = radiusOfTrainHeadWheel * cos(2 * M_PI * u);
			z = radiusOfTrainHeadWheel * sin(2 * M_PI * u);
			// Bottom vertex
			m_wheels[i].AddData(0, x, 0, z);
			m_wheels[i].AddData(1, 0, 1);
			m_wheels[i].AddData(2, u, 1);
			// Top vertex
			m_wheels[i].AddData(0, x, y, z);
			m_wheels[i].AddData(1, 0, 0);
			m_wheels[i].AddData(2, u, 0);

		}
		m_wheels[i].InitBuffers();
	}
	

	//
	// create the geometry
	//

	

	m_vb.AddAttribute(0,3); //position
	m_vb.AddAttribute(1, 3); //normals
	m_vb.AddAttribute(2, 2);  //tex coords
	

	//positions
	m_vb.AddData(0, -10, 0, -20);
	m_vb.AddData(0, 10, 0, -20);
	m_vb.AddData(0, -10, 0, 20);
	m_vb.AddData(0, 10, 0, 20);

	// normals
	m_vb.AddData(1, 0, 1, 0);
	m_vb.AddData(1, 0, 1, 0);
	m_vb.AddData(1, 0, 1, 0);
	m_vb.AddData(1, 0, 1, 0);

	// tex coords
	m_vb.AddData(2, 0, 0);
	m_vb.AddData(2, 10, 0);
	m_vb.AddData(2, 0, 20);
	m_vb.AddData(2, 10, 20);

	
	

	m_vb.AddIndex(1, 0, 2);
	m_vb.AddIndex(1, 2, 3);

	m_vb.InitBuffers();

	//line 1

	m_line1.AddAttribute(0, 3); //position
	m_line1.AddAttribute(1, 3); //normals
	m_line1.AddAttribute(2, 2);  //tex coords

	//position
	m_line1.AddData(0, -1.4, 0, 20);
	m_line1.AddData(0, -1.4, 0, -20);

	// normals
	m_line1.AddData(1, 0, 1, 0);
	m_line1.AddData(1, 0, 1, 0);
	//text cord
	m_line1.AddData(2, 0, 0);
	m_line1.AddData(2, 0, 0);

	m_line1.InitBuffers();

	//line 2 

	m_line2.AddAttribute(0, 3); //position
	m_line2.AddAttribute(1, 3); //normals
	m_line2.AddAttribute(2, 2);  //tex coords

	//position
	m_line2.AddData(0, -0.6, 0, 20);
	m_line2.AddData(0, -0.6, 0, -20);

	// normals
	m_line2.AddData(1, 0, 1, 0);
	m_line2.AddData(1, 0, 1, 0);
	//text cord
	m_line2.AddData(2, 0, 0);
	m_line2.AddData(2, 0, 0);

	m_line2.InitBuffers();

	//line 3

	m_line3.AddAttribute(0, 3); //position
	m_line3.AddAttribute(1, 3); //normals
	m_line3.AddAttribute(2, 2);  //tex coords

	//position
	m_line3.AddData(0, -0.4, 0, 20);
	m_line3.AddData(0, -0.4, 0, -20);

	// normals
	m_line3.AddData(1, 0, 1, 0);
	m_line3.AddData(1, 0, 1, 0);
	//text cord
	m_line3.AddData(2, 0, 0);
	m_line3.AddData(2, 0, 0);

	m_line3.InitBuffers();
	
	//line 4

	m_line4.AddAttribute(0, 3); //position
	m_line4.AddAttribute(1, 3); //normals
	m_line4.AddAttribute(2, 2);  //tex coords

	//position
	m_line4.AddData(0, 0.4, 0, 20);
	m_line4.AddData(0, 0.4, 0, -20);

	// normals
	m_line4.AddData(1, 0, 1, 0);
	m_line4.AddData(1, 0, 1, 0);
	//text cord
	m_line4.AddData(2, 0, 0);
	m_line4.AddData(2, 0, 0);

	m_line4.InitBuffers();

	//prism train head

	m_prism.AddAttribute(0, 3); //positions
	m_prism.AddAttribute(1, 3); //normals 
	m_prism.AddAttribute(2, 2); // tex coords

	add_triangle(
		glm::vec3(-0.25, 0, 0),
		
		glm::vec3(0.25, 0, 0),
		glm::vec3(0, 0.5, 0),
		m_prism
	);
	//m_prism.AddIndex(0, 1, 2);
	
	//back
	add_triangle(
		glm::vec3(0.25, 0, -1.5),
		glm::vec3(0, 0.5, -1.5),
		glm::vec3(-0.25, 0, -1.5),

		m_prism
	);

	//m_prism.AddIndex(3, 4, 5);
	add_triangle(
		glm::vec3(-0.25, 0, 0),
		glm::vec3(0, 0.5, 0),
		glm::vec3(0, 0.5, -1.5),
		
		m_prism
	);
	//m_prism.AddIndex(6, 7, 8);
	add_triangle(
		glm::vec3(-0.25, 0, 0),
		glm::vec3(0, 0.5, -1.5),
		glm::vec3(-0.25, 0, -1.5),
		
		
		m_prism
	);
	//m_prism.AddIndex(9, 10, 11);
	add_triangle(
		glm::vec3(0, 0.5, 0),
		glm::vec3(0.25, 0, 0),
		glm::vec3(0.25, 0, -1.5),
		m_prism
	);
	//m_prism.AddIndex(12, 13, 14);
	add_triangle(
		glm::vec3(0.25, 0, -1.5),
		glm::vec3(0, 0.5, -1.5),
		glm::vec3(0, 0.5, 0),
		m_prism
	);
	//m_prism.AddIndex(15, 16, 17);
	add_triangle(
		glm::vec3(-0.25, 0, 0),
		glm::vec3(0.25, 0, -1.5),
		glm::vec3(0.25, 0, 0),
		m_prism
	);

	add_triangle(
		glm::vec3(-0.25, 0, 0),
		glm::vec3(-0.25, 0, -1.5),
		glm::vec3(0.25, 0, -1.5),
		m_prism
	);
	

	

	m_prism.InitBuffers();

	//cabin prism

	m_cabin.AddAttribute(0, 3); //positions
	m_cabin.AddAttribute(1, 3); //normals 
	m_cabin.AddAttribute(2, 2); // tex coords

	//front
	add_triangle(
		glm::vec3(0.25, 0, 0),
		glm::vec3(0, 0.25, 0),
		glm::vec3(-0.25, 0, 0),
		
		m_cabin
	);
	//back
	add_triangle(
		glm::vec3(0.25, 0, -0.5),
		glm::vec3(0, 0.25, -0.5),
		glm::vec3(-0.25, 0, -0.5),
		m_cabin
	);

	//left side
	add_triangle(
		glm::vec3(-0.25, 0, 0),
		glm::vec3(0, 0.25, 0),
		glm::vec3(0, 0.25, -0.5),
		m_cabin
	);

	add_triangle(
		glm::vec3(-0.25, 0, 0),
		glm::vec3(0, 0.25, -0.5),
		glm::vec3(-0.25, 0, -0.5),
		m_cabin
	);

	//right side

	add_triangle(
		glm::vec3(0, 0.25, 0),
		glm::vec3(0.25, 0, 0),
		glm::vec3(0.25, 0, -0.5),
		m_cabin
	);

	add_triangle(
		glm::vec3(0, 0.25, 0),
		glm::vec3(0.25, 0, -0.5),
		glm::vec3(0, 0.25, -0.5),
		m_cabin
	);

	//bottom

	add_triangle(
		glm::vec3(0.25, 0, 0),
		glm::vec3(0.25, 0, -0.5),
		glm::vec3(-0.25, 0, 0),
		m_cabin
	);

	add_triangle(
		glm::vec3(-0.25, 0, 0),
		glm::vec3(0.25, 0, -0.5),
		glm::vec3(-0.25, 0, -0.5),
		m_cabin
	);

	

	m_cabin.InitBuffers();
	
	//wagon prism
	
	m_wagon.AddAttribute(0, 3); //positions
	m_wagon.AddAttribute(1, 3); //normals 
	m_wagon.AddAttribute(2, 2); // tex coords


	//front
	add_triangle(
		glm::vec3(0.3, 0, -1.5),
		glm::vec3(0, 0.75, -1.5),
		glm::vec3(-0.3, 0, -1.5),

		m_wagon
	);
	//back
	add_triangle(
		glm::vec3(0.3, 0, -3.5),
		glm::vec3(0, 0.75, -3.5),
		glm::vec3(-0.3, 0, -3.5),
		m_wagon
	);

	//left side
	add_triangle(
		glm::vec3(-0.3, 0, -1.5),
		glm::vec3(0, 0.75, -1.5),
		glm::vec3(0, 0.75, -3.5),
		m_wagon
	);

	add_triangle(
		glm::vec3(-0.3, 0, -1.5),
		glm::vec3(0, 0.75, -3.5),
		glm::vec3(-0.3, 0, -3.5),
		m_wagon
	);

	//right side

	add_triangle(
		glm::vec3(0, 0.75, -1.5),
		glm::vec3(0.3, 0, -1.5),
		glm::vec3(0.3, 0, -3.5),
		m_wagon
	);

	add_triangle(
		glm::vec3(0, 0.75, -1.5),
		glm::vec3(0.3, 0, -3.5),
		glm::vec3(0, 0.75, -3.5),
		m_wagon
	);

	//bottom

	add_triangle(
		glm::vec3(0.3, 0, -1.5),
		glm::vec3(0.3, 0, -3.5),
		glm::vec3(-0.3, 0, -1.5),
		m_wagon
	);

	add_triangle(
		glm::vec3(-0.3, 0, -1.5),
		glm::vec3(0.3, 0, -3.5),
		glm::vec3(-0.3, 0, -3.5),
		m_wagon
	);

	m_wagon.InitBuffers();


	//
	// load shaders	
	//
	m_program.AttachShader(GL_VERTEX_SHADER, "dirLight.vert");
	m_program.AttachShader(GL_FRAGMENT_SHADER, "dirLight.frag");

	m_program.BindAttribLoc(0, "vs_in_pos");
	m_program.BindAttribLoc(1, "vs_in_normal");
	m_program.BindAttribLoc(2, "vs_in_tex0");

	if (!m_program.LinkProgram())
	{
		return false;
	}

	//
	// misc init
	//

	m_camera.SetProj(45.0f, 640.0f / 480.0f, 0.01f, 1000.0f);
	
	m_texTrain = TextureFromFile("scrap.jpg");
	m_cowtext = GenTexture(139, 69, 19);
	m_texWheel = TextureFromFile("wheels.jpg");
	m_texture_gen = GenTexture(201,226,205);
	
	for (int i = 0; i < 6; i++)
	{
		cows[i] = ObjParser::parse("cow.obj");
		cows[i]->initBuffers();
	}
	

	return true;
}

void CMyApp::Clean()
{
	glDeleteTextures(1, &m_texture_gen);
	glDeleteTextures(1, &m_texWheel);
	glDeleteTextures(1, &m_texTrain);
	glDeleteTextures(1, &m_cowtext);
	m_program.Clean();
}

void CMyApp::Update()
{

	static Uint32 last_time = SDL_GetTicks();
	float delta_time = (SDL_GetTicks() - last_time) / 1000.0f;

	m_camera.Update(delta_time);

	last_time = SDL_GetTicks();
	
	if (speedFactor < 2.5f)
		wheeldelay = 0.025;
	else if (speedFactor == 4)
		wheeldelay = 0.006;
	else if (speedFactor >= 2.5 && speedFactor <= 3.1)
		wheeldelay = 0.030;
	else if (speedFactor >= 3.3 && speedFactor <= 3.6)
		wheeldelay = 0.031;
	
}


void CMyApp::Render()
{
	// clear the frame buffer (GL_COLOR_BUFFER_BIT) and z-buffer (GL_DEPTH_BUFFER_BIT)
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	m_program.On();
	{
		m_program.SetUniform("Ld", m_Ld);
		float cutoff = glm::cos(glm::radians(10.0));
		float  outercutoff = glm::cos(glm::radians(45.0));
		m_program.SetUniform("cutoff",cutoff );
		m_program.SetUniform("outercutoff", outercutoff);
	}
	m_program.Off();

	m_program.On();

	//plane

	
	glm::mat4 matWorld = glm::mat4(1.0f);
	glm::mat4 matWorldIT = glm::transpose(glm::inverse(matWorld));
	glm::mat4 mvp = m_camera.GetViewProj() * matWorld;

	m_program.SetUniform("world", matWorld);
	m_program.SetUniform("worldIT", matWorldIT);
	m_program.SetUniform("MVP", mvp);
	m_program.SetUniform("eye_pos", m_camera.GetEye());
	m_program.SetTexture("texImage", 0, m_texture_gen);

	m_vb.On();

	m_vb.DrawIndexed(GL_TRIANGLES, 0, 6, 0); //IB

	m_vb.Off();

	//wheel cover



	//wagon wheel

	for (int i = 0; i < 8; i++) {
		if (depart) {
			double angle = SDL_GetTicks() / 100;
			if (i < 4)
				matWorld = glm::mat4(1.0f) * glm::translate<float>(glm::vec3(-0.4, 0.3, -1.75 - (i * 0.25) - (i * wheelspace)        + zpos)) *
				glm::rotate<float>(-M_PI / 2, glm::vec3(1, 0, 0)) *
				glm::rotate<float>(-M_PI / 2, glm::vec3(0, 0, 1)) *
				glm::rotate<float>(angle, glm::vec3(0, 1, 0));
			else
				matWorld = glm::mat4(1.0f) * glm::translate<float>(glm::vec3(-0.7, 0.3, -1.75 - ((i - 4) * 0.25) - ((i - 4) * wheelspace)   + zpos)) *
				glm::rotate<float>(-M_PI / 2, glm::vec3(1, 0, 0)) *
				glm::rotate<float>(-M_PI / 2, glm::vec3(0, 0, 1)) *
				glm::rotate<float>(angle, glm::vec3(0, 1, 0));
		}
		else {
			if (i < 4)
				matWorld = glm::mat4(1.0f) * glm::translate<float>(glm::vec3(-0.4, 0.3, -1.75 - (i * 0.25) - (i * wheelspace)              )) *
				glm::rotate<float>(-M_PI / 2, glm::vec3(1, 0, 0)) *
				glm::rotate<float>(-M_PI / 2, glm::vec3(0, 0, 1));
			else
				matWorld = glm::mat4(1.0f) * glm::translate<float>(glm::vec3(-0.7, 0.3, -1.75 - ((i - 4) * 0.25) - ((i - 4) * wheelspace)       )) *
				glm::rotate<float>(-M_PI / 2, glm::vec3(1, 0, 0)) *
				glm::rotate<float>(-M_PI / 2, glm::vec3(0, 0, 1));
		}

		matWorldIT = glm::transpose(glm::inverse(matWorld));
		mvp = m_camera.GetViewProj() * matWorld;

		m_program.SetUniform("world", matWorld);
		m_program.SetUniform("worldIT", matWorldIT);
		m_program.SetUniform("MVP", mvp);
		m_program.SetUniform("eye_pos", m_camera.GetEye());
		m_program.SetUniform("viewpoint", m_camera.GetViewProj());

		m_program.SetUniform("wagonWheel", 1);
		//m_program.SetTexture("Wheel", 0, m_texWheel);
		m_WagonWheels[i].On();

		m_WagonWheels[i].Draw(GL_QUAD_STRIP, 0, 722);

		m_WagonWheels[i].Off();
	}
	m_program.SetUniform("wagonWheel", -1);

	//train head wheel
	for (int i = 0; i < 6; i++) {
		if (depart) {
			double angle = SDL_GetTicks() / 100;
			if (i < 3)
				matWorld = glm::mat4(1.0f) * glm::translate<float>(glm::vec3(-0.4, 0.3, -0.15 - (i * 0.3) - (i * wheelspace)              + zpos)) *
				glm::rotate<float>(-M_PI / 2, glm::vec3(1, 0, 0)) *
				glm::rotate<float>(-M_PI / 2, glm::vec3(0, 0, 1)) *
				glm::rotate<float>(angle, glm::vec3(0, 1, 0));
			else
				matWorld = glm::mat4(1.0f) * glm::translate<float>(glm::vec3(-0.7, 0.3, -0.15 - ((i - 3) * 0.3) - ((i - 3) * wheelspace)              + zpos)) *
				glm::rotate<float>(-M_PI / 2, glm::vec3(1, 0, 0)) *
				glm::rotate<float>(-M_PI / 2, glm::vec3(0, 0, 1)) *
				glm::rotate<float>(angle, glm::vec3(0, 1, 0));
		}
		else {
			if (i < 3)
				matWorld = glm::mat4(1.0f) * glm::translate<float>(glm::vec3(-0.4, 0.3,  -0.15 - ( i*0.3 )  - (i*wheelspace)            )) *
				glm::rotate<float>(-M_PI / 2, glm::vec3(1, 0, 0)) *
				glm::rotate<float>(-M_PI / 2, glm::vec3(0, 0, 1));
			else
				matWorld = glm::mat4(1.0f) * glm::translate<float>(glm::vec3(-0.7, 0.3, -0.15 - ( (i-3)*0.3 )  - ( (i-3) *wheelspace)           )) *
				glm::rotate<float>(-M_PI / 2, glm::vec3(1, 0, 0)) *
				glm::rotate<float>(-M_PI / 2, glm::vec3(0, 0, 1));
		}

		matWorldIT = glm::transpose(glm::inverse(matWorld));
		mvp = m_camera.GetViewProj() * matWorld;

		m_program.SetUniform("world", matWorld);
		m_program.SetUniform("worldIT", matWorldIT);
		m_program.SetUniform("MVP", mvp);
		m_program.SetUniform("eye_pos", m_camera.GetEye());

		m_program.SetUniform("wheel", 1);
		m_program.SetTexture("texWheel", 0, m_texWheel);
		m_wheels[i].On();

		m_wheels[i].Draw(GL_QUAD_STRIP, 0, 722); //722

		m_wheels[i].Off();
	}
	m_program.SetUniform("wheel", -1);


	//line1

	matWorld = glm::mat4(1.0f);// *glm::translate<float>(glm::vec3(0, 0.1, 0));
	matWorldIT = glm::transpose(glm::inverse(matWorld));
	mvp = m_camera.GetViewProj() * matWorld;
	glm::mat4 trans = glm::mat4(1.0f) * glm::translate<float>(glm::vec3(0, 0.25, 0));
	m_program.SetUniform("world", matWorld);
	m_program.SetUniform("worldIT", matWorldIT);
	m_program.SetUniform("MVP", mvp);
	m_program.SetUniform("trans", trans);

	m_program.SetUniform("line1", 1);
	m_program.SetUniform("elevate", 1);
	m_line1.On();
	m_line1.Draw(GL_LINES, 0, 2);
	m_line1.Off();
	m_program.SetUniform("line1", -1);
	m_program.SetUniform("elevate", -1);

	//line 2

	matWorld = glm::mat4(1.0f);
	matWorldIT = glm::transpose(glm::inverse(matWorld));
	mvp = m_camera.GetViewProj() * matWorld;

	m_program.SetUniform("world", matWorld);
	m_program.SetUniform("worldIT", matWorldIT);
	m_program.SetUniform("MVP", mvp);

	m_program.SetUniform("line1", 1);
	m_program.SetUniform("elevate", 1);
	m_line2.On();
	m_line2.Draw(GL_LINES, 0, 2);
	m_line2.Off();
	m_program.SetUniform("line1", -1);
	m_program.SetUniform("elevate", -1);

	//line 3

	matWorld = glm::mat4(1.0f);
	matWorldIT = glm::transpose(glm::inverse(matWorld));
	mvp = m_camera.GetViewProj() * matWorld;

	m_program.SetUniform("world", matWorld);
	m_program.SetUniform("worldIT", matWorldIT);
	m_program.SetUniform("MVP", mvp);

	m_program.SetUniform("line1", 1);
	m_program.SetUniform("elevate", 1);
	m_line3.On();
	m_line3.Draw(GL_LINES, 0, 2);
	m_line3.Off();
	m_program.SetUniform("line1", -1);
	m_program.SetUniform("elevate", -1);

	//line 4
	matWorld = glm::mat4(1.0f);
	matWorldIT = glm::transpose(glm::inverse(matWorld));
	mvp = m_camera.GetViewProj() * matWorld;

	m_program.SetUniform("world", matWorld);
	m_program.SetUniform("worldIT", matWorldIT);
	m_program.SetUniform("MVP", mvp);

	m_program.SetUniform("line1", 1);
	m_program.SetUniform("elevate", 1);
	m_line4.On();
	m_line4.Draw(GL_LINES, 0, 2);
	m_line4.Off();
	m_program.SetUniform("line1", -1);
	m_program.SetUniform("elevate", -1);

	
	//train head prism
	
	if (depart) {
		
		float deltaT = SDL_GetTicks() - time;
		if (deltaT > 20)
			deltaT = 15;
		float moving = speedFactor * deltaT / 1000;
	
		zpos += moving;
		m_program.SetUniform("pointlightpos1", glm::vec3(-0.5, 0.4,-0.25+zpos) );
		m_program.SetUniform("pointlightpos2", glm::vec3(-0.6, 0.4, -0.25+zpos));

		matWorld = glm::translate(glm::vec3(-0.5, 0.4, zpos));
		if (zpos > 20) {
			depart = false;
			zpos = 0;
			speedFactor = 2.5;
			m_program.SetUniform("pointlightpos1", glm::vec3(-0.5, 0.4, -0.25));
			m_program.SetUniform("pointlightpos2", glm::vec3(-0.6, 0.4, -0.25));
		}
		time = SDL_GetTicks();;
	}
	else
	matWorld = glm::translate(glm::vec3(-0.5, 0.4, 0)); // how high off the ground


	matWorldIT = glm::transpose(glm::inverse(matWorld));
	mvp = m_camera.GetViewProj() * matWorld;

	m_program.SetUniform("world", matWorld);
	m_program.SetUniform("worldIT", matWorldIT);
	m_program.SetUniform("MVP", mvp);
	m_program.SetUniform("eye_pos", m_camera.GetEye());

	
	m_program.SetUniform("octa_id", 1);
	m_program.SetTexture("trainPart", 0, m_texTrain);
	m_prism.On();
	m_prism.Draw(GL_TRIANGLES, 0, 24);
	m_prism.Off();
	m_program.SetUniform("octa_id", -1);


	//cabin
	
	if (depart) {
		
		matWorld = glm::translate(glm::vec3(-0.5, 0.9, zpos));
		
	}
	else
	matWorld = glm::translate(glm::vec3(-0.5, 0.9, 0)); // how high off the ground


	matWorldIT = glm::transpose(glm::inverse(matWorld));
	mvp = m_camera.GetViewProj() * matWorld;

	m_program.SetUniform("world", matWorld);
	m_program.SetUniform("worldIT", matWorldIT);
	m_program.SetUniform("MVP", mvp);
	m_program.SetUniform("eye_pos", m_camera.GetEye());


	m_program.SetUniform("octa_id", 1);
	m_program.SetTexture("trainPart", 0, m_texTrain);
	m_cabin.On();
	m_cabin.Draw(GL_TRIANGLES, 0, 24);
	m_cabin.Off();
	m_program.SetUniform("octa_id", -1);
	
	//wagon
	
	if (depart) {
		matWorld = glm::translate(glm::vec3(-0.5, 0.4,zpos));
	}
	else
	matWorld = glm::translate(glm::vec3(-0.5, 0.4, 0)); // how high off the ground


	matWorldIT = glm::transpose(glm::inverse(matWorld));
	mvp = m_camera.GetViewProj() * matWorld;

	m_program.SetUniform("world", matWorld);
	m_program.SetUniform("worldIT", matWorldIT);
	m_program.SetUniform("MVP", mvp);
	m_program.SetUniform("eye_pos", m_camera.GetEye());


	m_program.SetUniform("wagon_id", 1);
	m_program.SetTexture("trainPart", 0, m_texTrain);
	m_wagon.On();
	m_wagon.Draw(GL_TRIANGLES, 0, 24);
	m_wagon.Off();
	m_program.SetUniform("wagon_id", -1);

	//cow
	
	int cowPos = -8;
	for (int i = 0; i < 6; i++)
	{
		if (i % 2==0) 
			matWorld = glm::translate<float>(glm::vec3(cowPos, 0.35, sqrt(81 - (cowPos) * (cowPos)))) *
				glm::rotate<float>(-M_PI / 2, glm::vec3(1, 0, 0)) *
				glm::rotate<float>(-M_PI / 2, glm::vec3(0, 0, 1)) *
				glm::scale(glm::vec3(0.1));
		else
			matWorld = glm::translate<float>(glm::vec3(cowPos, 0.35, -sqrt(81 - (cowPos) * (cowPos)))) *
			glm::rotate<float>(-M_PI / 2, glm::vec3(1, 0, 0)) *
			glm::rotate<float>(-M_PI / 2, glm::vec3(0, 0, 1)) *
			glm::scale(glm::vec3(0.1));
			
		if (cow_number == i + 1)
			matWorld *= glm::translate<float>(glm::vec3(10, 0, 0));  // cuz we scale down to 0.1 

			matWorldIT = glm::transpose(glm::inverse(matWorld));
			mvp = m_camera.GetViewProj() * matWorld;

		m_program.SetUniform("world", matWorld);
		m_program.SetUniform("worldIT", matWorldIT);
		m_program.SetUniform("MVP", mvp);
		m_program.SetUniform("eye_pos", m_camera.GetEye());
		m_program.SetUniform("cow", 1);
		m_program.SetTexture("cowText", 0, m_cowtext);
		cows[i]->draw();
		cowPos += 3;
	}

	m_program.SetUniform("cow", -1);


	//head wheel cover

	for (int i = 0; i < 12; i++)
	{

		if (depart) 
		{
			double angle = SDL_GetTicks() / 100;
			if (i < 6) {
				if (i % 2 == 0)
				{
					matWorld = glm::translate(glm::vec3(-0.3, 0.3, -wheeldelay -0.15 - ((i / 2) * 0.3) - ((i / 2) * wheelspace)       + zpos )) *
						glm::rotate<float>(-M_PI / 2, glm::vec3(1, 0, 0)) *
						glm::rotate<float>(-M_PI / 2, glm::vec3(0, 0, 1)) * 
						glm::rotate<float>(angle, glm::vec3(0, 1, 0));
				}
				else
				{
					matWorld = glm::translate(glm::vec3(-0.4, 0.3, -wheeldelay -0.15 - ((i / 2) * 0.3) - ((i / 2) * wheelspace)     + zpos)) *
						glm::rotate<float>(-M_PI / 2, glm::vec3(1, 0, 0)) *
						glm::rotate<float>(-M_PI / 2, glm::vec3(0, 0, 1)) *
						glm::rotate<float>(angle, glm::vec3(0, 1, 0));
				}
			}
			else
			{
				if (i % 2 == 0)
				{
					matWorld = glm::translate(glm::vec3(-0.6, 0.3, -wheeldelay -0.15 - (((i - 6) / 2) * 0.3) - (((i - 6) / 2) * wheelspace)      + zpos)) *
						glm::rotate<float>(-M_PI / 2, glm::vec3(1, 0, 0)) *
						glm::rotate<float>(-M_PI / 2, glm::vec3(0, 0, 1)) *
						glm::rotate<float>(angle, glm::vec3(0, 1, 0));
				}
				else
				{
					matWorld = glm::translate(glm::vec3(-0.7, 0.3, -wheeldelay -0.15 - (((i - 6) / 2) * 0.3) - (((i - 6) / 2) * wheelspace)     + zpos)) *
						glm::rotate<float>(-M_PI / 2, glm::vec3(1, 0, 0)) *
						glm::rotate<float>(-M_PI / 2, glm::vec3(0, 0, 1)) *
						glm::rotate<float>(angle, glm::vec3(0, 1, 0));
				}
			}
		}
		else
		{
			if (i < 6) {
				if (i % 2 == 0)
				{
					matWorld = glm::translate(glm::vec3(-0.3, 0.3, -0.15 - ((i / 2) * 0.3) - ((i / 2) * wheelspace))) *
						glm::rotate<float>(-M_PI / 2, glm::vec3(1, 0, 0)) *
						glm::rotate<float>(-M_PI / 2, glm::vec3(0, 0, 1)); 
				}
				else
				{
					matWorld = glm::translate(glm::vec3(-0.4, 0.3, -0.15 - ((i / 2) * 0.3) - ((i / 2) * wheelspace))) *
						glm::rotate<float>(-M_PI / 2, glm::vec3(1, 0, 0)) *
						glm::rotate<float>(-M_PI / 2, glm::vec3(0, 0, 1));
				}
			}
			else
			{
				if (i % 2 == 0)
				{
					matWorld = glm::translate(glm::vec3(-0.6, 0.3, -0.15 - (((i - 6) / 2) * 0.3) - (((i - 6) / 2) * wheelspace))) *
						glm::rotate<float>(-M_PI / 2, glm::vec3(1, 0, 0)) *
						glm::rotate<float>(-M_PI / 2, glm::vec3(0, 0, 1)); 
				}
				else
				{
					matWorld = glm::translate(glm::vec3(-0.7, 0.3, -0.15 - (((i - 6) / 2) * 0.3) - (((i - 6) / 2) * wheelspace))) *
						glm::rotate<float>(-M_PI / 2, glm::vec3(1, 0, 0)) *
						glm::rotate<float>(-M_PI / 2, glm::vec3(0, 0, 1));
				}
			}
		}
		


		matWorldIT = glm::transpose(glm::inverse(matWorld));
		mvp = m_camera.GetViewProj() * matWorld;

		m_program.SetUniform("world", matWorld);
		m_program.SetUniform("worldIT", matWorldIT);
		m_program.SetUniform("MVP", mvp);
		m_program.SetUniform("eye_pos", m_camera.GetEye());



		m_program.SetUniform("wheel", 1);
		m_program.SetTexture("texWheel", 0, m_texWheel);
		m_wheelCovers[i].On();
		m_wheelCovers[i].Draw(GL_POLYGON, 0, 361);
		m_wheelCovers[i].Off();
	}

	m_program.SetUniform("wheel", -1);
	
	//wagon wheel cover
	
	
	for (int i = 0; i < 16; i++)
	{

		if (depart)
		{
			double angle = SDL_GetTicks() / 100;
			if (i < 8) {
				if (i % 2 == 0)
				{
					matWorld = glm::translate(glm::vec3(-0.3, 0.3, - wheeldelay -1.75 - ((i / 2) * 0.25) - ((i / 2) * wheelspace) + zpos)) *
						glm::rotate<float>(-M_PI / 2, glm::vec3(1, 0, 0)) *
						glm::rotate<float>(-M_PI / 2, glm::vec3(0, 0, 1)) *
						glm::rotate<float>(angle, glm::vec3(0, 1, 0));
				}
				else
				{
					matWorld = glm::translate(glm::vec3(-0.4, 0.3, - wheeldelay -1.75 - ((i / 2) * 0.25) - ((i / 2) * wheelspace) + zpos)) *
						glm::rotate<float>(-M_PI / 2, glm::vec3(1, 0, 0)) *
						glm::rotate<float>(-M_PI / 2, glm::vec3(0, 0, 1)) *
						glm::rotate<float>(angle, glm::vec3(0, 1, 0));
				}
			}
			else
			{
				if (i % 2 == 0)
				{
					matWorld = glm::translate(glm::vec3(-0.6, 0.3, -wheeldelay -1.75 - (((i - 8) / 2) * 0.25) - (((i - 8) / 2) * wheelspace) + zpos)) *
						glm::rotate<float>(-M_PI / 2, glm::vec3(1, 0, 0)) *
						glm::rotate<float>(-M_PI / 2, glm::vec3(0, 0, 1)) *
						glm::rotate<float>(angle, glm::vec3(0, 1, 0));
				}
				else
				{
					matWorld = glm::translate(glm::vec3(-0.7, 0.3, -wheeldelay -1.75 - (((i - 8) / 2) * 0.25) - (((i - 8) / 2) * wheelspace) + zpos)) *
						glm::rotate<float>(-M_PI / 2, glm::vec3(1, 0, 0)) *
						glm::rotate<float>(-M_PI / 2, glm::vec3(0, 0, 1)) *
						glm::rotate<float>(angle, glm::vec3(0, 1, 0));
				}
			}
		}
		else
		{
			if (i < 8) {
				if (i % 2 == 0)
				{
					matWorld = glm::translate(glm::vec3(-0.3, 0.3, -1.75 - ((i / 2) * 0.25) - ((i / 2) * wheelspace))) *
						glm::rotate<float>(-M_PI / 2, glm::vec3(1, 0, 0)) *
						glm::rotate<float>(-M_PI / 2, glm::vec3(0, 0, 1));
				}
				else
				{
					matWorld = glm::translate(glm::vec3(-0.4, 0.3, -1.75 - ((i / 2) * 0.25) - ((i / 2) * wheelspace))) *
						glm::rotate<float>(-M_PI / 2, glm::vec3(1, 0, 0)) *
						glm::rotate<float>(-M_PI / 2, glm::vec3(0, 0, 1));
				}
			}
			else
			{
				if (i % 2 == 0)
				{
					matWorld = glm::translate(glm::vec3(-0.6, 0.3, -1.75 - (((i - 8) / 2) * 0.25) - (((i - 8) / 2) * wheelspace))) *
						glm::rotate<float>(-M_PI / 2, glm::vec3(1, 0, 0)) *
						glm::rotate<float>(-M_PI / 2, glm::vec3(0, 0, 1));
				}
				else
				{
					matWorld = glm::translate(glm::vec3(-0.7, 0.3, -1.75 - (((i - 8) / 2) * 0.25) - (((i - 8) / 2) * wheelspace))) *
						glm::rotate<float>(-M_PI / 2, glm::vec3(1, 0, 0)) *
						glm::rotate<float>(-M_PI / 2, glm::vec3(0, 0, 1));
				}
			}
		}



		matWorldIT = glm::transpose(glm::inverse(matWorld));
		mvp = m_camera.GetViewProj() * matWorld;

		m_program.SetUniform("world", matWorld);
		m_program.SetUniform("worldIT", matWorldIT);
		m_program.SetUniform("MVP", mvp);
		m_program.SetUniform("eye_pos", m_camera.GetEye());



		m_program.SetUniform("wagonWheel", 1);
		m_wagonWheelCovers[i].On();
		m_wagonWheelCovers[i].Draw(GL_POLYGON, 0, 361);
		m_wagonWheelCovers[i].Off();
	}
	
	m_program.SetUniform("wagonWheel", -1);



	m_program.Off();
}

void CMyApp::KeyboardDown(SDL_KeyboardEvent& key)
{
	m_camera.KeyboardDown(key);
	if (key.keysym.sym >= SDLK_1 && key.keysym.sym <= SDLK_6)
	{
		cow_number = key.keysym.sym - SDLK_0;
		
	}
	if (key.keysym.sym == SDLK_SPACE)
	{
		depart = true;
		time = SDL_GetTicks();
	}
	if (key.keysym.sym == SDLK_KP_PLUS)
	{
		speedFactor += 0.5;
		if (speedFactor >= 4)
			speedFactor = 4;
	
	}
	if (key.keysym.sym == SDLK_KP_MINUS)
	{		
		speedFactor -= 0.5;
		if (speedFactor < 1)
			speedFactor = 1;
	
	}
}

void CMyApp::KeyboardUp(SDL_KeyboardEvent& key)
{
	m_camera.KeyboardUp(key);
}

void CMyApp::MouseMove(SDL_MouseMotionEvent& mouse)
{
	m_camera.MouseMove(mouse);
}

void CMyApp::MouseDown(SDL_MouseButtonEvent& mouse)
{
}

void CMyApp::MouseUp(SDL_MouseButtonEvent& mouse)
{
}

void CMyApp::MouseWheel(SDL_MouseWheelEvent& wheel)
{
}

// _w: new width, _h: new height
void CMyApp::Resize(int _w, int _h)
{
	glViewport(0, 0, _w, _h);

	m_camera.Resize(_w, _h);
}