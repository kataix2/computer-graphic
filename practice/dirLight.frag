#version 130

in vec3 vs_out_pos;
in vec3 vs_out_normal;
in vec2 vs_out_tex0;
in vec3 vs_out_color;

out vec4 fs_out_col;

uniform vec4 viewpoint;

//
// uniform variables
//

uniform int wagon_id = -1;

uniform int octa_id = -1;

uniform int line1 = -1; 
uniform int line2 = -1;
uniform int cow = -1;
uniform int wheel = -1; 
uniform int wagonWheel = -1;

// scene attributes
uniform vec4 La = vec4(0.2f, 0.2f, 0.2f, 1); // how bright 
uniform vec3 eye_pos = vec3(0, 15, 15); // view pos 

// light attributes
uniform vec3 light_dir = vec3( 0, -1, 0 );
uniform vec3 light_pos = vec3(0,4,0);	// light position 

uniform vec3 pointlightpos1 = vec3 (-0.5,0.4,-0.25);
uniform vec3 pointlightpos2 = vec3 (-0.6,0.4,-0.25);

uniform vec4 pointlightcolor = vec4(1, 0.773, 0.561,1);
uniform vec4 pointlightcolor2 = vec4(1, 0.773, 0.561,1);

uniform vec4 Ld = vec4(0.788f, 0.886f, 1, 1); // color
uniform vec4 Ls = vec4(1, 1, 1, 1);

// material attributes
uniform vec4 Ka = vec4(1, 1, 1, 1); // material ambient
//uniform vec4 Kd = vec4 (0.30, 0.30, 0.30, 1); // material diffuse
uniform vec4 Kd = vec4(0.70,0.70,0.70,1);
//uniform vec4 Kd = vec4(0.5,0.5,0.5,0.3);
uniform vec4 Ks = vec4(1, 1, 1, 1); // specular color


uniform float specular_power = 16; // shininess , specularStrength 

uniform sampler2D texImage; // ground
uniform sampler2D trainPart; // trainpart
uniform sampler2D texWheel; // train wheel
uniform sampler2D cowtext; // cow

uniform float outercutoff;
uniform float cutoff; 

vec4 CalDirLight();
vec4 CalPointLight();
vec4 CalSecondPointLight();

void main()
{


	vec4 res = CalDirLight();
	res += CalPointLight();
	res += CalSecondPointLight();
	
	if(octa_id != -1)
	{
		
		fs_out_col = res * texture(trainPart,vs_out_tex0.st);
	}
	else if(line1 != -1)
	{
		fs_out_col = vec4 (0,0,0,1) ; 
	}
	else if(line2 != -1)
	{
		fs_out_col = vec4 (0,0,0,1);
	}
	else if(cow != -1)
	{
		
		fs_out_col = res * texture(cowtext,vs_out_tex0.st);
	}
	else if(wagon_id != -1)
	{
		float sc = gl_FragCoord.y / 480.0f;
		fs_out_col = mix(vec4 (0.439216, 0.576471, 0.858824,1), vec4(0.678431, 0.917647,0.917647,1) , sc );
	}
	else if(wheel != -1)
	{
		
		fs_out_col = res * texture(texWheel,vs_out_tex0.st);
	}
	else if(wagonWheel != -1)
	{
		fs_out_col = vec4 (0.60, 0.40, 0.12,1.0);
	}
	else{
		
		fs_out_col = res * texture(texImage,vs_out_tex0.st);
	}  
	
	
	
}


vec4 CalDirLight()
{
	// ambient

	vec4 ambient = La * Ka; // light ambient * material ambient

	// diffuse

	vec3 normal = normalize( vs_out_normal );
	vec3 toLight = normalize(-light_dir) ;
	//vec3 toLight = normalize(vec3(0,2,-8)-vs_out_pos);
	float di = clamp( dot( toLight, normal), 0.0f, 1.0f ); // diffent between normal and light direction
	vec4 diffuse = Ld*Kd*di; // light diffuse * meterial diffuse * diffent

	// specular

	vec4 specular = vec4(0);

	if ( di > 0 )
	{
		vec3 e = normalize( eye_pos - vs_out_pos ); // vector pointing toward the camera
		vec3 r = normalize( reflect( -toLight, normal ) ); // reflect ray
		float si = pow( clamp( dot(e, r), 0.0f, 1.0f ), specular_power ); //specular component
		specular = Ls*Ks*si; // light specular * metarial specular * spec 
	} 


	return ambient+diffuse+specular;

}
vec4 CalPointLight(){

	// ambient
	
	vec4 ambient = La * Ka; // light ambient * material ambient

	// diffuse
	
	vec3 normal = normalize( vs_out_normal );
	vec3 toLight = normalize(pointlightpos1-vs_out_pos);
	float di = clamp( dot( toLight, normal), 0.0f, 1.0f ); // diffent between normal and light direction
	vec4 diffuse = pointlightcolor*Kd*di; // light diffuse * meterial diffuse * diffent

	//
	// specular
	//
	vec4 specular = vec4(0);

	if ( di > 0 )
	{
		vec3 e = normalize( eye_pos - vs_out_pos ); // vector pointing toward the camera
		vec3 r = normalize( reflect( -toLight, normal ) ); // reflect ray
		float si = pow( clamp( dot(e, r), 0.0f, 1.0f ), specular_power ); //specular component
		specular = Ls*Ks*si; // light specular * metarial specular * spec 
	} 

	// Attenuation 
	float distance = length(pointlightpos1 - vs_out_pos);
	float attenuation = 1.0f / (1.0f + 0.09 * distance + 0.032 * (distance*distance) );
	ambient *= attenuation;
	diffuse *= attenuation;
	specular *= attenuation;

	vec3 dir = vec3 (0,0,-1);
	float theta = dot(toLight,normalize(dir));
	float epsilon = (cutoff - outercutoff);
	float intensity = clamp((theta - outercutoff) / epsilon,0,1);

	
	diffuse *= intensity;
	specular *= intensity;

	return ambient+diffuse+specular;
}

vec4 CalSecondPointLight()
{
	// ambient
	
	vec4 ambient = La * Ka; // light ambient * material ambient

	// diffuse
	
	vec3 normal = normalize( vs_out_normal );
	vec3 toLight = normalize(pointlightpos2-vs_out_pos);
	float di = clamp( dot( toLight, normal), 0.0f, 1.0f ); // diffent between normal and light direction
	vec4 diffuse = pointlightcolor2*Kd*di; // light diffuse * meterial diffuse * diffent

	//
	// specular
	//
	vec4 specular = vec4(0);

	if ( di > 0 )
	{
		vec3 e = normalize( eye_pos - vs_out_pos ); // vector pointing toward the camera
		vec3 r = normalize( reflect( -toLight, normal ) ); // reflect ray
		float si = pow( clamp( dot(e, r), 0.0f, 1.0f ), specular_power ); //specular component
		specular = Ls*Ks*si; // light specular * metarial specular * spec 
	} 

	// Attenuation 
	float distance = length(pointlightpos1 - vs_out_pos);
	float attenuation = 1.0f / (1.0f + 0.09 * distance + 0.032 * (distance*distance) );
	ambient *= attenuation;
	diffuse *= attenuation;
	specular *= attenuation;

	//spotlight
	vec3 dir = vec3 (0,0,-1);
	float theta = dot(toLight,normalize(dir));
	float epsilon = (cutoff - outercutoff);
	float intensity = clamp((theta - outercutoff) / epsilon,0,1);
	
	
	diffuse *= intensity;
	specular *= intensity;

	return ambient+diffuse+specular;
}